package com.obsqurazone.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URI;

public class SampleTest {

    @Test
    public void uitest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(CapabilityType.BROWSER_NAME,"chrome");
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(URI.create("http://localhost:4444/wd/hub").toURL(), caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.navigate().to("https://www.swtestacademy.com");

        WebElement logo = driver.findElement(By.className("fusion-logo-link"));

        System.out.println(logo.isDisplayed());
        driver.quit();
    }
}
